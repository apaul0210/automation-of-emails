﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmtpServerClient
{
    class InsertIntoDB
    {
        //class for creating of emails for various uses and insert them into db table for reuse only use for emails for logging of users ie regsitration and website activity.
        public InsertIntoDB()
        {

        }
        public void CreateEmailTable()
        {
            var CreateTable = String.Format("CREATE TABLE IF NOT EXISTS t_emails_logging(f_Message varchar(200),f_sent int(5) auto_increment");
            using (var connection = new MySql.Data.MySqlClient.MySqlConnection("Server=localhost;" + "database=test;" + "username=root;" + "password=;"))
            {
                connection.Open();
                MySql.Data.MySqlClient.MySqlCommand Create = new MySql.Data.MySqlClient.MySqlCommand(CreateTable, connection);
                MySql.Data.MySqlClient.MySqlDataReader reader;
                reader = Create.ExecuteReader();
                if (reader.HasRows == false) Create.ExecuteNonQuery();
                connection.Close();
            }
        }
        public void InsertEmails()
        {
            try
            {
                using (var Connection = new MySql.Data.MySqlClient.MySqlConnection("Server=localhost;" + "Database=test;" + "username=root;" + "password=;"))
                {
                    Connection.Open();
                    var Message = new Message();
                    
                        var WelcomeMessage = Message.CreateWelcomeMessage().Body;
                        var FailedLogins = Message.FailedLoginsEmail().Body;
                        var Deactivate = Message.AccountDeactivationEmail().Body;

                        var query = String.Format("INSERT INTO t_emails_logging T(T.F_Message,T.F_SENT) VALUES({0},{1})", WelcomeMessage, null);
                        var query1 = String.Format("INSERT INTO t_emails_logging T(T.F_Message,T.F_SENT) VALUES ({0},{1})", FailedLogins, null);
                        var query2 = String.Format("INSERT INTO t_emails_logging T(T.F_Message, T_.F_SENT) VALUES ({0},{1})", Deactivate, null);

                        MySql.Data.MySqlClient.MySqlCommand command = new MySql.Data.MySqlClient.MySqlCommand(query, Connection);
                        MySql.Data.MySqlClient.MySqlCommand command1 = new MySql.Data.MySqlClient.MySqlCommand(query1, Connection);
                        MySql.Data.MySqlClient.MySqlCommand command2 = new MySql.Data.MySqlClient.MySqlCommand(query2, Connection);

                        command.CommandText = query;
                        command1.CommandText = query1;
                        command2.CommandText = query2;

                        command.ExecuteNonQuery();
                        command1.ExecuteNonQuery();
                        command2.ExecuteNonQuery();

                        command1.Dispose();
                        command2.Dispose();
                        command.Dispose();
                    
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Message did not insert into required table for logging purposes" + exception.Message);
                MySql.Data.MySqlClient.MySqlException mysqlException = null;
                Console.WriteLine("" + mysqlException.Message);
            }
        }
    }
}
