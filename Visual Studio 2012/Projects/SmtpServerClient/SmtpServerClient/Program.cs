﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Diagnostics;

namespace SmtpServerClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //consider implementing a complete smtp client for use within framework. !!!!
                using (System.Net.Mail.SmtpClient temp = new SmtpClient("localhost", 25))
                {
                    String to = "postmaster@localhost.com";
                    String Subject = "This is test Message.";
                    String from = "postmaster@localhost.com";
                    String body = "Test Message to check whether local smtp server is working";
                    MailMessage message = new MailMessage(to, from,Subject,body);
                    temp.EnableSsl = false;
                    message.Subject = Subject;
                    message.Body = body;
                    temp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    temp.Send(message);
                    Console.WriteLine("Email has been sent");
                    Console.WriteLine("");   
                }
            }
            catch (System.Net.Mail.SmtpFailedRecipientException exception)
            {
                Console.WriteLine("{0} Email did not send" + exception.Message.ToString());
                Console.WriteLine("" + exception.StackTrace);
                Console.WriteLine("" + exception.InnerException);
                Console.WriteLine();
                Console.WriteLine();
            }

        }
    }
}
